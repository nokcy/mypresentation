ANDROID STUDIO BASIS
####################

*LAYOUT
*JAVA
*My java code

```

package com.example.android.justjava;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * this method increase the number
     */
        public void increment(View view) {
           int quantity=3;
            display(quantity);
        }



    /**
     * this method decrease the number
     */
    public void decrement(View view) {
        int quantity=1;
        display(quantity);
    }





    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(
                R.id.display_me);
        quantityTextView.setText(number);
    }


}






```